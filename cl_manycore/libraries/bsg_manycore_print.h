#ifndef BSG_MANYCORE_PRINT_H
#define BSG_MANYCORE_PRINT_H

#include <stdint.h>

void hb_mc_print_hex (uint8_t *p);

#endif
